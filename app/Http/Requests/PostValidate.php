<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostValidate extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'title' => 'required|string|min:10|max:255',
            'content' => 'required|string|min:10|max:3000',
            'category_id' => 'required|exists:categories,id',
            'image' => 'file|image'
        ];
    }
}
