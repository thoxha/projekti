<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Http\Requests\PostValidate;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class PostsController extends Controller
{
    public function index(Request $request)
    {
        $data['search_field']  = filter_var($request->input('search_field'), FILTER_SANITIZE_STRING);
        $data['posts'] = Post::orderBy('id', 'desc')->paginate(24);
        return view('posts/index', $data);

    }

    public function show($id)
    {
        $post = Post::findOrFail($id);

        return view('posts/show', ['post' => $post]);
    }



    public function postform($id = 0) {
        $data['categories'] = Category::orderBy('category_order', 'asc')->get();
        if ($id == 0) {
            $data['post'] = (object) [
                'id' => 0,
                'title' => "",
                'content' => "",
                'category_id' => 0
            ];

        } else {
            $data['post'] = Post::where('id', $id)->first();
        }
        return view('admin/postform', $data);
    }



    public function editpost(PostValidate $request)
    {

        $post_id = (int) $request->input('post_id');
        if ($post_id == 0) {
            $post = new Post;
        } else {
            $post = Post::where('id', $post_id)->first();
        }

        $post->user_id = Auth::id();
        $post->category_id = filter_var($request->input('category_id'), FILTER_SANITIZE_NUMBER_INT);
        $post->content = $request->input('content');
        $post->title = filter_var($request->input('title'), FILTER_SANITIZE_STRING);


        if($request->file('image') != NULL) {
            $post->image = $request->file('image')->store('images');
        }

        if ($post->save()) {
            return redirect()->route('admin.index')->with(['status' => "Postimi u regjistrua!"]);
        } else {
            return redirect()->route('admin.index')->with(['status' => "GABIM! Postimi nuk u regjistrua!"]);
        }
    }


    public function search(Request $request) {
        $data['search_field'] = filter_var($request->input('search_field'), FILTER_SANITIZE_STRING);
        $data['posts'] = Post::where('title', 'like', "%".$data['search_field']."%")->where('content', 'like', "%".$data['search_field']."%")->paginate(24);
        return view('posts.index', $data);
    }

    public function add_comment(Request $request) {
        $post_id = (int) $request->input('post_id');
        $content = filter_var($request->input('comment'), FILTER_SANITIZE_STRING);
        $comment = new Comment;
        $comment->comment_content = $content;
        $comment->user_id = Auth::id();
        $comment->post_id = $post_id;
        $comment->save();

        return back();

    }

    public function delete_comment(Comment $comment) {
        Gate::authorize('delete-comment', $comment);
        $comment->delete();
        return back();
    }

    public function edit_comment(Comment $comment) {
        Gate::authorize('edit-comment', $comment);
        $post = $comment->post;
        return view('posts/edit_comment', ['comment' => $comment, 'post' => $post]);
    }

    public function update_comment(Request $request) {
        $comment_id = (int) $request->input('comment_id');
        $comment_content = filter_var($request->input('comment'), FILTER_SANITIZE_STRING);
        $comment = Comment::find($comment_id);

        Gate::authorize('edit-comment', $comment);
        $comment->comment_content = $comment_content;
        $comment->save();

        return redirect()->route('posts.show', [$comment->post]);
    }
}
