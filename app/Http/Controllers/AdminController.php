<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index() {
        return view('admin/index');
    }

    public function postindex() {
        $data['posts'] = Post::orderBy('id', 'desc')->paginate(5);
        return view('admin/postindex', $data);
    }

    public function postdelete(Request $request) {
        $posts = $request->input('delete');
        if ($posts != NULL) {
            foreach($posts as $key => $post) {
                $id = (int) $key;
                $post =Post::where('id', $id)->first();
                if ($post != NULL) {
                    // Fshiji komentet
                    foreach($post->comments as $comment) {
                        $comment->delete();
                    }

                    // Fshije imazhin
                    $image = $post->image;
                    Storage::delete($image);

                    // Fshije postimin
                    $post->delete();
                }

            }
        }

        return back();
    }
}
