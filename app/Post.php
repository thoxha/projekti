<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function comments() {
        return $this->hasMany('App\Comment');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function setTitleAttribute($title) {
        $this->attributes['title'] = $title;
        $this->save();
        $this->attributes['slug'] = Str::slug($this->attributes['id']."-".$title);
    }
}
