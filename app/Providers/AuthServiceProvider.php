<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('edit-comment', function($user, $comment) {
            if (Auth::user()->level == 3) {
                return true;
            }

           return $user->id == $comment->user_id;
        });

        Gate::define('delete-comment', function($user, $comment) {
            if (Auth::user()->level == 3) {
                return true;
            }
            return $user->id == $comment->user_id;
        });
    }
}
