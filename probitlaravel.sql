-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 03, 2020 at 06:09 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `probitlaravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_order`) VALUES
(1, 'Politikë', 1),
(2, 'Ekonomi', 2),
(3, 'Sport', 5),
(4, 'Fun', 4),
(5, 'Kuriozitete', 3);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment_content`, `user_id`, `post_id`, `created_at`, `updated_at`) VALUES
(3, 'pershendetje, ky eshte komenti im per kete lajm. edited', 2, 12, '2020-06-03 14:58:30', '2020-06-03 15:35:16'),
(4, 'Ish-kryeministri i Kosovës, Albin Kurti, iu është drejtuar qytetarëve përmes një video, pas votimit të Qeverisë Hoti.\r\n\r\nKurti ka numëruar disa nga sukseset e qeverisjes së tij, teksa ka shtuar se Qeveria Hoti është  Qeveri koti.', 1, 12, '2020-06-03 15:35:15', '2020-06-03 15:35:15'),
(5, '***** ds fg gds gsdg', 3, 12, '2020-06-03 15:41:24', '2020-06-03 15:43:05');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_05_29_160849_create_posts_table', 2),
(5, '2020_05_29_162251_create_categories_table', 2),
(6, '2020_05_29_162603_create_comments_table', 2),
(7, '2020_05_29_172603_update-posts-table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `user_id`, `deleted_at`, `image`, `category_id`, `created_at`, `updated_at`, `slug`) VALUES
(2, 'Origjina e coronavirusit nuk është nga tregu në Wuhan, pohojnë shkencëtarët', '<p>Tregu në Wuhan nuk ishte shkak po “pre” e coronavirusit, thotë Qendra kineze e Kontrollit dhe e Parandalimit të Sëmundjeve. Sipas mediave të huaja, përcjell Telegrafi, provat konfirmojnë se virusi posedon origjinën nga lakuriqët kinezë përpara se të transmetohej tek njerëzit, mirëpo vendndodhja e saktë nuk dihet ende.</p>', 1, NULL, NULL, 1, '2020-05-29 17:16:06', '2020-06-03 14:36:27', '2-origjina-e-coronavirusit-nuk-eshte-nga-tregu-ne-wuhan-pohojne-shkencetaret'),
(5, 'Nga shoqëria civile kërkojnë që LDK të publikojë marrëveshjen me Listën Serbe para votimit të qeverisë\n', 'Marrëveshjet për bashkëqeverisje të LDK-së me AAK-në dhe NISMA-n janë bërë publike, por jo ajo edhe me Listën Serbe.\n\nNga kjo parti janë arsyetuar se marrëveshjet i kanë publikuar vet partnerët e koalicionit të ardhshëm dhe se ajo vet nuk ka publikuar asnjë marrëveshje.', 1, NULL, NULL, 1, '2020-05-29 15:55:56', '2020-05-29 15:55:56', '5-nga-shoqeria-civile-kerkojne-qe-ldk-te-publikoje-marreveshjen-me-listen-serbe-para-votimit-te-qeverise'),
(11, 'Origjina e coronavirusit nuk është nga tregu në Wuhan, pohojnë shkencëtarët', '<p>Tregu në Wuhan nuk ishte shkak po “pre” e coronavirusit, thotë Qendra kineze e Kontrollit dhe e Parandalimit të Sëmundjeve. Sipas mediave të huaja, përcjell Telegrafi, provat konfirmojnë se virusi posedon origjinën nga lakuriqët kinezë përpara se të transmetohej tek njerëzit, mirëpo vendndodhja e saktë nuk dihet ende.</p>', 1, NULL, NULL, 1, '2020-05-29 17:16:06', '2020-06-03 14:36:19', '11-origjina-e-coronavirusit-nuk-eshte-nga-tregu-ne-wuhan-pohojne-shkencetaret'),
(13, 'Origjina e coronavirusit nuk është nga tregu në Wuhan, pohojnë shkencëtarët', '<p>Tregu në Wuhan nuk ishte shkak po “pre” e coronavirusit, thotë Qendra kineze e Kontrollit dhe e Parandalimit të Sëmundjeve. Sipas mediave të huaja, përcjell Telegrafi, provat konfirmojnë se virusi posedon origjinën nga lakuriqët kinezë përpara se të transmetohej tek njerëzit, mirëpo vendndodhja e saktë nuk dihet ende.</p>', 1, NULL, NULL, 1, '2020-05-29 17:16:06', '2020-06-03 14:36:10', '13-origjina-e-coronavirusit-nuk-eshte-nga-tregu-ne-wuhan-pohojne-shkencetaret');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `level`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Tahir Hoxha', 'tahir.hoxha@gmail.com', 3, NULL, '$2y$10$sbpdnvNBAPuWyQdJ8ikTK.uVVc5DufgNvhtSpyoEfWcDNbs.xnsKu', NULL, '2020-05-29 14:06:07', '2020-05-29 14:06:07'),
(2, 'Taulant Bajrami', 'taulantbb@gmail.com', 1, NULL, '$2y$10$YpI.vaLmhRYFpP29BY.h0eX0qKbLyBTuN2Lt4ioqlMdER/OU0dAJa', NULL, '2020-06-03 14:58:04', '2020-06-03 14:58:04'),
(3, 'Sadullah', 'scabraa@gmail.com', 0, NULL, '$2y$10$XtAhTPskvtLr0Zu5ZKE/xOG8NmCek4LqJPe9im37AzFcpH71230Tm', NULL, '2020-06-03 15:40:22', '2020-06-03 15:40:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_category_name_index` (`category_name`),
  ADD KEY `categories_category_order_index` (`category_order`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_index` (`user_id`),
  ADD KEY `comments_post_id_index` (`post_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_title_index` (`title`),
  ADD KEY `posts_user_id_index` (`user_id`),
  ADD KEY `posts_category_id_index` (`category_id`),
  ADD KEY `posts_slug_index` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_level_index` (`level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
