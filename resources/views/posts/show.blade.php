@extends('layouts.app')

@section('content')
    <h1>{{ $post->title }}</h1>
    @if ($post->image != "" )
        <div>
            <img class="img-fluid" src="/{{ $post->image }}">
        </div>
    @endif
    <div>{!! $post->content  !!}</div>
    <div>Data e postimit: {{ date("d.m.Y H:i:s", strtotime($post->created_at)) }}</div>
    <p class="text-right">{{ $post->user->name }}

    <h2>Komentet</h2>

    @if (Auth::check())
        <form method="POST" action="{{ route('posts.add_comment') }}">
            @csrf
            <input type="hidden" name="post_id" value="{{ $post->id }}">
            <textarea name="comment" id="comment" class="form-control" rows="10"></textarea>
            <input type="submit" class="btn btn-primary" value="SEND">
        </form>
    @endif

    <div>
        @foreach($post->comments as $comment)
            <div style="border: 2px solid black; padding: 1em">
                <p>{{ $comment->user->name }}</p>
                <p>Komentuar me: {{ date("d.m.Y H:i:s", strtotime($comment->created_at)) }}</p>
                <p>Modifikuar me: {{ date("d.m.Y H:i:s", strtotime($comment->updated_at)) }}</p>
                <div>
                    {{ $comment->comment_content }}
                </div>
                @if (Gate::allows('edit-comment', $comment))
                    <a href="{{ route('posts.edit_comment', [$comment]) }}" class="btn btn-info">Edit</a>
                @endif
                @if (Gate::allows('delete-comment', $comment))
                    <a href="{{ route('posts.delete_comment', [$comment]) }}" class="btn btn-danger">Delete</a>
                @endif
            </div>

        @endforeach

    </div>
@endsection
