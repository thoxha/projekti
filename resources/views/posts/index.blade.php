@extends('layouts.app')

@section('content')
    <div class="row">
        @foreach($posts as $post)
            <div class="col-md-4">
                @if ($post->image != "" )
                <a href="{{ route('posts.show', [$post]) }}">
                <img class="img-fluid" src="/{{ $post->image }}">
                </a>
                @endif
                <a href="{{ route('posts.show', [$post]) }}">{{ $post->title }}</a>
            </div>
        @endforeach
    </div>


    <div>
        {{ $posts->appends(['search_field' => $search_field])->links() }}
    </div>
@endsection
