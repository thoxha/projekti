@extends('layouts.app')

@section('content')
    <h1>Edit comment</h1>

    @if (Auth::check())
        <form method="POST" action="{{ route('posts.update_comment') }}">
            @csrf
            <input type="hidden" name="post_id" value="{{ $post->id }}">
            <input type="hidden" name="comment_id" value="{{ $comment->id }}">
            <textarea name="comment" id="comment" class="form-control" rows="10">{{ $comment->comment_content }}</textarea>
            <input type="submit" class="btn btn-primary" value="SEND">
        </form>
    @endif
@endsection
