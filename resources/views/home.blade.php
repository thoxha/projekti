@extends('layouts.app')

@section('head')
    <style>
        h1 {
            color: green;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <h1>Faqja kryesore</h1>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('js')
    <script>
        $(document).ready(function() {
           //
        });

    </script>
@endsection
