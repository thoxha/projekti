@extends('layouts.admin')

@section('content')
    <form method="post" action="{{ route('admin.postdelete') }}">
        @csrf
        <table class="table">
            @foreach($posts as $post)
                <tr>
                    <td><input type="checkbox" name="delete[{{ $post->id }}]"></td>
                    <td><a href="{{ route('admin.postform', ['id' => $post->id]) }}">{{ $post->title }}</a></td>
                </tr>
            @endforeach
        </table>

        <input type="submit" class="btn btn-danger" value="FSHIJI">
    </form>
    <div>{{ $posts->links() }}</div>
@endsection
