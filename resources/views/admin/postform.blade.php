@extends('layouts.admin')

@section('content')
    <h1>Editimi i lajmit</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form method="POST" action="{{ route('admin.editpost') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="post_id" value="{{ $post->id }}">
        <label for="title">Shëno titullin:</label>
        <input type="text" name="title" id="title" class="form-control" value="{{ old('title', $post->title) }}">

        <label for="content">Shëno postimin:</label>
        <textarea name="content" id="content" class="form-control" rows="10">{{ old('content', $post->content) }}</textarea>

        <label for="category_id">Zgjedhe rubrikën:</label>
        <select name="category_id" id="category_id" class="form-control">
            <option value="0">Zgjedhe rubriken</option>
            @foreach($categories as $category)
                @if ($category->id == old('category_id', $post->category_id))
                    <option selected value="{{ $category->id }}">{{ $category->category_name }}</option>
                @else
                    <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                @endif
            @endforeach
        </select>

        <label for="image">Zgjedhe fotografinë:</label>
        <input type="file" name="image" id="image" class="form-control">

        <input type="submit" value="RUAJE" class="btn btn-primary">
    </form>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/ckeditor5/19.1.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#content' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
@endsection
