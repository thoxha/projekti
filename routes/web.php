<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('posts')->group(function () {
    Route::get('index', 'PostsController@index')->name('posts.index');
    Route::get('show/{id}', 'PostsController@show')->name('posts.show');
    Route::get('search', 'PostsController@search')->name('posts.search');
    Route::post('add_comment', 'PostsController@add_comment')->name('posts.add_comment')->middleware('auth');
    Route::post('update_comment', 'PostsController@update_comment')->name('posts.update_comment')->middleware('auth');
    Route::get('edit_comment/{comment}', 'PostsController@edit_comment')->name('posts.edit_comment')->middleware('auth');
    Route::get('delete_comment/{comment}', 'PostsController@delete_comment')->name('posts.delete_comment')->middleware('auth');
});

Route::prefix('member')->middleware('auth')->group(function () {

});

Route::prefix('admin')->middleware('admin')->group(function () {
    Route::get('index', 'AdminController@index')->name('admin.index');
    Route::get('postindex', 'AdminController@postindex')->name('admin.postindex');
    Route::post('postdelete', 'AdminController@postdelete')->name('admin.postdelete');

    Route::get('postform/{id?}', 'PostsController@postform')->name('admin.postform');
    Route::post('editpost', 'PostsController@editpost')->name('admin.editpost');
});
